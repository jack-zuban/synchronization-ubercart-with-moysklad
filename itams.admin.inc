<?php

/**
 * @file
 * Administration page callbacks for the itams module.
 */

/**
 * Form builder. Configure Moy Sklad Settings.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function itams_admin_settings() {
  $form['itams_user_login'] = array(
    '#title' => t('User name'),
    '#type' => 'textfield',
    '#description' => t('Insert the user name for MoySklad'),
    '#default_value' => variable_get('itams_user_login', ''),
    '#size' => 20,
    '#weight' => -10,
  );

  $form['itams_user_password'] = array(
    '#title' => t('User password'),
    '#type' => 'textfield',
    '#description' => t('The user password in the MoySklad'),
    '#default_value' => variable_get('itams_user_password', ''),
    '#size' => 20,
    '#weight' => -9,
  );

  $form['itams_store_id'] = array(
    '#title' => t('Warehouses UUID'),
    '#type' => 'textfield',
    '#description' => t('The Unique ID in the MoySklad system'),
    '#default_value' => variable_get('itams_store_id', ''),
    '#size' => 50,
    '#weight' => -8,
  );

  $options_active = array(
    'active' => t('Module is active'),
  );

  $form['itams_active_mode'] = array(
    '#title' => t('Enable synchronization with MoySklad'),
    '#type' => 'checkboxes',
    '#description' => t('Check this field for activating the synchronization.'),
    '#options' => $options_active,
    '#default_value' => variable_get('itams_active_mode', ''),
    '#weight' => -25,
  );

  $orders_active = array(
    'active' => t('Upload orders'),
  );

  $form['itams_active_orders_mode'] = array(
    '#title' => t('Upload Orders to Moy Sklad'),
    '#type' => 'checkboxes',
    '#description' => t('Check this field for upload the new orders to Moy Sklad automatically.'),
    '#options' => $orders_active,
    '#default_value' => variable_get('itams_active_orders_mode', ''),
    '#weight' => -5,
  );

  // fieldset for setup the uploading
  $form['itams_orders_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Uploads setup'),
    '#states' => array(
      'visible' => array(
        ':input[name="itams_active_orders_mode[active]"]' => array('checked' => TRUE),
      ),
    ),
  );

  //the contractor's UUID
  $form['itams_orders_settings']['itams_counterparty_uuid'] = array(
    '#title' => t('Contractors UUID'),
    '#type' => 'textfield',
    '#description' => t('The unique UUID for working with orders in the Moy Sklad system'),
    '#default_value' => variable_get('itams_counterparty_uuid', ''),
    '#size' => 50,
    '#weight' => -4,
  );

  //The organizations UUID
  $form['itams_orders_settings']['itams_organization_uuid'] = array(
    '#title' => t('Organizations UUID'),
    '#type' => 'textfield',
    '#description' => t('The unique organizations UUID for working with orders in the Moy Sklad system'),
    '#default_value' => variable_get('itams_organization_uuid', ''),
    '#size' => 50,
    '#weight' => -3,
  );

  $form['#submit'][] = 'itams_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Process itams settings submission.
 */
function itams_admin_settings_submit($form, $form_state) {
  variable_set('itams_user_login', $form_state['values']['itams_user_login']);
  variable_set('itams_user_password', $form_state['values']['itams_user_password']);
  variable_set('itams_store_id', $form_state['values']['itams_store_id']);
  variable_set('itams_active_mode', $form_state['values']['itams_active_mode']);
  variable_set('itams_active_orders_mode', $form_state['values']['itams_active_orders_mode']);
  variable_set('itams_counterparty_uuid', $form_state['values']['itams_counterparty_uuid']);
  variable_set('itams_organization_uuid', $form_state['values']['itams_organization_uuid']);
}

/**
 * Validate the form.
 */
function itams_admin_settings_validate($form, &$form_state) {
  if ($form_state['values']['itams_active_mode']['active'] === 'active') {
    if ($form_state['values']['itams_user_login'] == '') {
      form_set_error('itams_user_login', t('User name cant be empty'));
    }

    if ($form_state['values']['itams_user_password'] == '') {
      form_set_error('itams_user_password', t('User password cant be ampty'));
    }

    //orders fields checks
    if ($form_state['values']['itams_active_orders_mode']['active'] === 'active') {
      if ($form_state['values']['itams_counterparty_uuid'] == '') {
        form_set_error('itams_counterparty_uuid', t('Contractors UUID cant be empty'));
      }

      if ($form_state['values']['itams_organization_uuid'] == '') {
        form_set_error('itams_organization_uuid', t('Organizations UUID cant be empty'));
      }
    }
  }
}

